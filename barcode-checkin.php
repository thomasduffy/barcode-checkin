<?php
/*
Plugin Name: Barcode Check-in
Description: Check-in system for CoWorking Space using barcode reader.
Plugin URI: http://firegoby.jp/
Author: Takayuki Miyauchi
Version: 0.1.0
*/

define('CHECKIN_TABLE', 'checkin');

require_once(dirname(__FILE__).'/includes/class-addrewriterules.php');
require_once(dirname(__FILE__).'/includes/role.class.php');
require_once(dirname(__FILE__).'/includes/functions.php');

register_activation_hook(__FILE__, array('BarcodeCheckIn', 'activation'));
register_deactivation_hook(__FILE__, array('BarcodeCheckIn', 'deactivation'));

new BarcodeCheckIn();

class BarcodeCheckIn {

const version = '0.1.0';
const user_meta = 'checkin_key';
const key_length = 12;
const last_checkin = 'last_checkin';
const id_created = 'id_created';

function __construct()
{
    add_action("plugins_loaded", array(&$this, "plugins_loaded"));
    add_filter("user_contactmethods", array(&$this, "user_contactmethods"));
    add_action("profile_update", array(&$this, "profile_update"));
    add_action("show_user_profile", array(&$this, "user_profile"));
    add_action("edit_user_profile", array(&$this, "user_profile"));

    new WP_AddRewriteRules(
        'check-in\/barcode$',
        'draw_barcode',
        array(&$this, "draw_barcode")
    );

    new WP_AddRewriteRules(
        'check-in$',
        'display_checkin',
        array(&$this, "display_checkin")
    );
}

public function user_profile()
{
    if (isset($_GET['user_id']) && intval($_GET['user_id'])) {
        $uid = $_GET['user_id'];
    } else {
        $uid = get_current_user_id();
    }
    echo "<h3>バーコード</h3>";
    echo sprintf(
        '<p><img alt="" src="/check-in/barcode/?nonce=%s&key=%s" /></p>',
        wp_create_nonce(self::user_meta),
        self::create_checkin_key($uid)
    );
}

public function profile_update($uid)
{
    if (isset($_POST[self::user_meta]) && !strlen($_POST[self::user_meta])) {
        if (current_user_can('administrator')) {
            self::create_checkin_key($uid);
        }
    }
}

public function user_contactmethods($methods)
{
    if (current_user_can('administrator')) {
        $methods[self::user_meta] = 'チェックインID';
        $methods[self::id_created] = 'チェックインID作成日';
    }
    return $methods;
}

public function activation()
{
    flush_rewrite_rules();

    $role = get_role('administrator');
    $role->add_cap('check-in');

    add_role('front_staff', 'Front Staff', array(
        'check-in' => true,
    ));
    add_role('member', 'Member', array(
    ));

    global $wpdb;
    $table = $wpdb->prefix.CHECKIN_TABLE;
    if ($wpdb->get_var("show tables like '$table'") !== $table) {
        $sql = "CREATE TABLE `{$table}` (
            `id` bigint(20) unsigned not null auto_increment,
            `user_id` bigint(20) unsigned,
            `checkined` bigint not null,
            primary key (`id`),
            key `user_id` (`user_id`),
            key `checkined` (`checkined`)
            );";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

public function deactivation()
{
    $role = get_role('administrator');
    $role->remove_cap('check-in');
    flush_rewrite_rules();
}

public function plugins_loaded()
{
    define('BC_CHECKIN_URI', plugins_url('', __FILE__));
    define('BC_CHECKIN_DIR', dirname(__FILE__));

    if (is_admin()) {
        require_once(dirname(__FILE__).'/includes/admin.php');
    }
}

public function display_checkin()
{
    nocache_headers();

    if (!is_user_logged_in()) {
        $redirect = home_url('wp-login.php');
        $redirect = add_query_arg('redirect_to', home_url('check-in'), $redirect);
        wp_redirect($redirect);
        exit;
    }

    $html = '<!doctype html>';
    $html .= '<head>';
    if (get_option('wfb_apple_icon')) {
        if (get_option('wfb_apple_icon_precomposed')) {
            $link = '<link rel="apple-touch-icon-precomposed" href="%s" />'."\n";
        } else {
            $link = '<link rel="apple-touch-icon" href="%s" />'."\n";
        }
        $html .= sprintf($link, esc_url(trim(get_option("wfb_apple_icon"))));
    }
    $html .= '<meta name="apple-mobile-web-app-capable" content="yes" />';
    $html .= '<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0" />';
    $css = plugins_url(
        'css/check-in.css?ver='.filemtime(dirname(__FILE__).'/css/check-in.css'),
        __FILE__
    );
    $html .= sprintf(
        '<link rel="stylesheet" href="%s" type="text/css" media="all" />',
        apply_filters(
            'barcode-check-in-style',
            $css
        )
    );
    $html .= '<title>Check-in - '.get_bloginfo('name').'</title>';
    $html .= '<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>';
    $html .= '</head>';
    $html .= '<body>';

    $tpl = '<div class="%s">%s</div>';

    $html .= '<div id="check-in">';
    $html .= '<div id="check-in-body" class="container-fluid">';
    $html .= '<div class="row-fluid">';
    $user = get_userdata(get_current_user_id());
    $html .= sprintf(
        $tpl,
        'user_gravatar span5',
        get_avatar(
            $user->user_email,
            96
        )
    );
    $html .= sprintf(
        $tpl,
        'logo span7',
        $this->get_logo()
    );
    global $wp_roles;
    $roles = array_values($user->roles);
    $meta = array(
        '<span class="user_full_name">'.esc_html($user->last_name).' '.esc_html($user->first_name).'</span>',
        esc_html($wp_roles->roles[$roles[0]]['name'])
    );
    $html .= sprintf(
        $tpl,
        'user_meta span7',
        join('<br />', $meta)
    );
    $html .= '</div><!-- .row-fluid -->';
    $html .= '<div class="row-fluid">';
    $html .= sprintf(
        $tpl,
        'barcode span12',
        $this->get_barcode()
    );
    $html .= '</div><!-- .row-fluid -->';
    $html .= '</div><!-- #check-in-body -->';
    $html .= '</div><!-- #check-in -->';

    $html .=<<<EOL
<script type="text/javascript">
function setCenter(){
    if ($('#check-in').length) {
        var h = (jQuery(window).height()+100) / 2;
        jQuery('body').height(jQuery(window).height()+100);
        jQuery('#check-in').css('top', h+'px');
        setTimeout("scrollTo(0,50)", 1);
    }
};
$(document).ready(function(){
    setCenter();
});
$(window).resize(function(){
    setCenter();
});
$('.button-primary').attr('class', 'btn btn-primary');
</script>
EOL;
    $html .= '</body>';
    $html .= '</html>';
    echo $html;
    exit;
}

public function draw_barcode()
{
    if (is_user_logged_in() && isset($_GET['nonce'])
            && wp_verify_nonce($_GET['nonce'], self::user_meta)) {
        if (isset($_GET['key']) && $_GET['key']) {
            $key = $_GET['key'];
        } else {
            $key = $this->create_checkin_key();
        }
        if ($key) {
            nocache_headers();
            $inc =  ini_get('include_path');
            ini_set('include_path', $inc.':'.dirname(__FILE__).'/includes');
            require_once(dirname(__FILE__).'/includes/Image/Barcode2.php');
            $code = new Image_Barcode2();
            $code->draw($key, 'code128', 'png', true, 50, 2);
            exit;
        }
    }

    nocache_headers();
    wp_die('Not Authorized.');
}

private function get_logo()
{
    if ($logo = get_option("wfb_login_logo")) {
        $html = sprintf(
            '<h1><a href="%s"><img src="%s" alt="%s" /></a></h1>',
            esc_attr(home_url()),
            esc_attr($logo),
            esc_attr(get_bloginfo('name'))
        );
    } else {
        $html = sprintf(
            '<h1><a href="%s">%s</a></h1>',
            esc_attr(home_url()),
            esc_html(get_bloginfo('name'))
        );
    }
    return $html;
}

public function get_checkin_key()
{
    $current_user = wp_get_current_user();
    if ($current_user) {
        $uid = $current_user->ID;
        return get_user_meta($uid, self::user_meta, true);
    }
    return false;
}

private function create_checkin_key($uid = null)
{
    if (!$uid) {
        $current_user = get_userdata(get_current_user_id());
    } else {
        $current_user = get_userdata($uid);
    }

    if ($current_user) {
        $uid = $current_user->ID;

        if ($key = get_user_meta($uid, self::user_meta, true)) {
            return $key;
        } else {
            $key = $this->create_key();
            while ($this->get_user_by_key($key)) {
                $key = $this->create_key();
            }
            update_user_meta($uid, self::user_meta, $key);
            update_user_meta($uid, self::id_created, date_i18n('Y-m-d H:i:s'));
            return $key;
        }
    }
}

private function create_key()
{
    return substr(md5(uniqid(rand(), 1)), 0, self::key_length);
}

private function get_barcode()
{
    $barcode = sprintf(
        '<img src="%1$s/?nonce=%2$s" alt="" />',
        home_url('check-in/barcode'),
        wp_create_nonce(self::user_meta)
    );

    return $barcode;
}

public function get_user_by_key($key)
{
    $args = array(
        'meta_key' => self::user_meta,
        'meta_value' => $key
    );
    $users = get_users($args);
    if (isset($users[0]) && isset($users[0]->ID) && $users[0]->ID) {
        return $users[0]->ID;
    }
    return false;
}

} // class BarcodeCheckIn

