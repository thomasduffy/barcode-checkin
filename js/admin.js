(function($){
var reset_interval = 10000;
var timer;

$('#barcode').focus();
$('#barcode').change(function(){
    if (!$('#barcode').val().length) {
        return;
    }
    $.getJSON(api+'&id='+$('#barcode').val(), function(u){
        if (u.ID) {
            $('#display_name').text(u.data.display_name);
            $('#user_avatar').html(u.data.avatar);
            $('#member_type').text(u.data.member_type);
            $('#stayed').text(u.data.stayed);
            $('#history').html('');
            for (var i=0; i<u.data.history.length; i++) {
                $('#history').append('<li>'+u.data.history[i]+'</li>');
            }
            $('#user-info').fadeIn('slow', function(){
                $('#barcode').focus();
                $('#barcode').val('');
                autoclear();
                $(window).mousemove(autoclear);
            });
        } else {
            checkinError('バーコードが一致しません。');
        }
    });
});

function autoclear(){
    clearTimeout(timer);
    timer = setTimeout(reset_data, reset_interval);
}

function reset_data(){
    $('#user-info').fadeOut('slow', function(){
        $('#barcode').val('');
        $('#barcode').focus();
    });
    $(window).unbind('mousemove', autoclear);
}

function checkinError(msg){
    $('#alert').text(msg);
    $('#alert').fadeIn('fast').delay(2000).fadeOut('fast', function(){
        $(this).text('');
        $('#barcode').focus();
        $('#barcode').val('');
    });
}

})(jQuery);
